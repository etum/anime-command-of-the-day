#!/bin/bash
cd "${0%/*}"

RES=$(xrandr | grep '*' | head -n 1 | awk '{print $1}')
W=$(echo $RES | cut -d 'x' -f1)
H=$(echo $RES | cut -d 'x' -f2)
AnimeScale=675
CMD=$(find /bin/ /usr/bin/ /sbin/ /usr/sbin/ | shuf | head -n 1)
CMDlen=$(echo $CMD | wc -c )
Desc=$(whatis $CMD | cut -d ' ' -f3- | fold -w 53 -s)
Desclen=$(echo $Desc | wc -c )
Animegirl=$(find anime_girls/ -mindepth 1 | shuf | head -n 1)

if [ -z "$Desc" ]; then Desc="??????"; fi
if [ $CMDlen -ge 34 ]; then CPT=64; else CPT=84; fi

convert $Animegirl -geometry x$AnimeScale animegirl.png
convert bg1080.png -gravity southeast -draw "image Over 0,0 0,0 'animegirl.png'" bg.png
convert bg.png -font Anime-Ace -pointsize $CPT -annotate +24+120 $CMD bg.png
convert bg.png -font Anime-Ace -pointsize 46 -annotate +96+240 "$Desc" bg.png
if [ $W -ne 1920 -a $H -ne 1080 ]; then convert bg.png -resize $RES bg.png; fi
