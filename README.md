# Anime Command of the Day

A random command from your system and its usage being read by an anime girl holding a computer book. 
Would make a good i3lock screen.

## Dependencies 

["Anime Ace" font](https://www.fontget.com/font/anime-ace-family/)

imagemagick 

## Installation

git clone or download as a zip


## Usage

```./bg_build.sh```

### i3lock example 

```bindsym $mod+shift+m exec --no-startup-id ~/anime-command-of-the-day-master/bg_build.sh && i3lock -i ~/anime-command-of-the-day-master/bg.png```

## TODO / Issues 
* I need good transparent pngs (or even svgs) of anime girls with computer books. Pls send.


* This script currently only works for 16:9 screens. If you use a different aspect ratio monitor and would like this to work you can feel free to:
    1. Contribute code for scaling
    2. Gargle my nuts 


* Autosizing doesn't work with wayland because no xrandr. Resolution can be hardcoded like so  
    ```RES=1920x1080```

## Examples 

![ALT](/.examples/rm_example.png)
![ALT](/.examples/bgscaled.png)
